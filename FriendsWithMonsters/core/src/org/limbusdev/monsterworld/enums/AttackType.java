package org.limbusdev.monsterworld.enums;

/**
 * Created by georg on 06.12.15.
 */
public enum AttackType {
    MAGICAL, PHYSICAL
}
