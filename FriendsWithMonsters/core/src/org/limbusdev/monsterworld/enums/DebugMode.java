
package org.limbusdev.monsterworld.enums;

/*
 * Copyright (c) 2016 by Georg Eckert
 *
 * Licensed under GPL 3.0 https://www.gnu.org/licenses/gpl-3.0.en.html
 */

public enum DebugMode {
    RELEASE, WORLD, BATTLE, INVENTORY,
}
