package org.limbusdev.monsterworld.geometry;

/**
 * Created by georg on 23.01.16.
 */
public class IdentifiableRectangle {
    /* ............................................................................ ATTRIBUTES .. */
    public int x,y;
    public int ID;
    /* ........................................................................... CONSTRUCTOR .. */
    public IdentifiableRectangle(int x, int y, int ID) {
        this.x = x;
        this.y = y;
        this.ID = ID;
    }
    /* ............................................................................... METHODS .. */
    
    /* ..................................................................... GETTERS & SETTERS .. */
}
