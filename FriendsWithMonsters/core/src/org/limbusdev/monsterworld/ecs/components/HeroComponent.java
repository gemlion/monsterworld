package org.limbusdev.monsterworld.ecs.components;

import com.badlogic.ashley.core.Component;

/**
 * Identifies an Entity as the Hero
 * Created by georg on 25.01.16.
 */
public class HeroComponent implements Component{
    /* ............................................................................ ATTRIBUTES .. */
    
    /* ........................................................................... CONSTRUCTOR .. */
    
    /* ............................................................................... METHODS .. */
    
    /* ..................................................................... GETTERS & SETTERS .. */
}
