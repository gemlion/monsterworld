# Guardians

## Copyright

The monsters and sprites listed here are under Creative Commons 4.0 (CC-BY-4.0). This means you are free to use them anywhere with only a few strings attached. This is to make sure the monsters are known everywhere under the same names. Rules:

* always give proper credit to the author of the monsters: Georg Eckert
* always use the monster names provided here
* if you alter the sprites, make sure to still pass the original authors name on
* the license only applies to the sprites, not to your code
* otherwise do whatever you want

## Encyclo

This is a comprehensive list of all available Guarian Monsters.

|ID|Name|Sprite|16x16|3D|Description|Name Origin|
|-----------|-------------|--------------|-----------|-------------------------|--------------|------------|
|0_0|Egg|![](../Artwork/Spritesheets/Monsters/128x128/0_0.png)|![](../Artwork/Spritesheets/Monsters/16x16/0_0.png)|||
|1_0   |Fordin |![](../Artwork/Spritesheets/Monsters/128x128/1_0.png) |![](../Artwork/Spritesheets/Monsters/16x16/1_0.png)  |![](../Artwork/Spritesheets/Monsters/3d/1_0.png)   ||Forest dinosaur|
|1_1   |Stegofor   |![](../Artwork/Spritesheets/Monsters/128x128/1_1.png)  |![](../Artwork/Spritesheets/Monsters/16x16/1_1.png) |   ||Stegosaurus forest|
|1_2   |Brachifor   |![](../Artwork/Spritesheets/Monsters/128x128/1_2.png) |![](../Artwork/Spritesheets/Monsters/16x16/1_2.png)  |   ||Brachiosaurus forest|
|2_0   |Kroki   |![](../Artwork/Spritesheets/Monsters/128x128/2_0.png) |![](../Artwork/Spritesheets/Monsters/16x16/2_0.png)  |   ||Crocodile|
|2_1   |Krokivip   |![](../Artwork/Spritesheets/Monsters/128x128/2_1.png) |  |   ||Crocodile viper|
|2_2   |Leviadile   |![](../Artwork/Spritesheets/Monsters/128x128/2_2.png) |  |   ||Leviathan crocodile|
|3_0   |Devidin   |![](../Artwork/Spritesheets/Monsters/128x128/3_0.png) |![](../Artwork/Spritesheets/Monsters/16x16/3_0.png)  |   ||Devil dinosaur|
|3_1   |Devidra   |![](../Artwork/Spritesheets/Monsters/128x128/3_1.png) |![](../Artwork/Spritesheets/Monsters/16x16/3_1.png)  |![](../Artwork/Spritesheets/Monsters/3d/3_1.png)   ||Devil dragon|
|3_2   |Deviraptor   |![](../Artwork/Spritesheets/Monsters/128x128/3_2.png) |  |   ||Devil raptor|
|4_0   |Aerodin   |![](../Artwork/Spritesheets/Monsters/128x128/4_0.png)  | |   ||Air dinosaur|
|4_1   |Aerodeer   |![](../Artwork/Spritesheets/Monsters/128x128/4_1.png) |  |   ||Air deer|
|4_2   |Aerostag   |![](../Artwork/Spritesheets/Monsters/128x128/4_2.png) |  |   ||Air stag|
|5_0   |Weastoat   |![](../Artwork/Spritesheets/Monsters/128x128/5_0.png) |![](../Artwork/Spritesheets/Monsters/16x16/5_0.png)  |   ||Weasel toat|
|6_0   |Mooty   |![](../Artwork/Spritesheets/Monsters/128x128/6_0.png) |![](../Artwork/Spritesheets/Monsters/16x16/6_0.png)  |   ||Moon kitty|
|6_1   |Camoon   |![](../Artwork/Spritesheets/Monsters/128x128/6_1.png) |  |   ||Cat moon|
|6_2   |Moopard   |![](../Artwork/Spritesheets/Monsters/128x128/6_2.png) |  |   ||Moon leopard|
|7_0   |Wuppy   |![](../Artwork/Spritesheets/Monsters/128x128/7_0.png)  | |   ||Worm puppy|
|7_1   |Earog   |![](../Artwork/Spritesheets/Monsters/128x128/7_1.png) |  |   ||Ear dog|
|7_2   |Deemog   |![](../Artwork/Spritesheets/Monsters/128x128/7_2.png)  | |   ||Demon dog|
|8_0   |Dradder   |![](../Artwork/Spritesheets/Monsters/128x128/8_0.png) |  |   ||Dread adder|
|8_1   |Driper   |![](../Artwork/Spritesheets/Monsters/128x128/8_1.png)|   |   ||Dread viper|
|9_0   |Spreye   |![](../Artwork/Spritesheets/Monsters/128x128/9_0.png) |  |   ||Spring eye|
|9_1   |Buttereye   |![](../Artwork/Spritesheets/Monsters/128x128/9_1.png) |  |   ||Butterfly eye|
|10_0   |Duggot   |![](../Artwork/Spritesheets/Monsters/128x128/10_0.png)  | |   ||Double maggot|
|10_1   |Breem   |![](../Artwork/Spritesheets/Monsters/128x128/10_1.png)  | |   ||Bee worm|
|11_0   |Marvillar   |![](../Artwork/Spritesheets/Monsters/128x128/11_0.png)  | |   ||Marvellous caterpillar|
|11_1   |Marvantis   |![](../Artwork/Spritesheets/Monsters/128x128/11_1.png) |  |   ||Marvellous mantis|
|12_0   |Palmpot   |![](../Artwork/Spritesheets/Monsters/128x128/12_0.png)  | |   ||Palmtree pot|
|12_1   |Bonose   |![](../Artwork/Spritesheets/Monsters/128x128/12_1.png) |  |   ||Bonsai nose|
|13_0   |Erimat   |![](../Artwork/Spritesheets/Monsters/128x128/13_0.png)  | |   ||Hermit aspirant|
|13_1   |Erichief   |![](../Artwork/Spritesheets/Monsters/128x128/13_1.png) |  |   ||Hermit chief|
|14_0   |Eggatch   |![](../Artwork/Spritesheets/Monsters/128x128/14_0.png)  | |   ||Hatching egg|
|14_1   |Owlock   |![](../Artwork/Spritesheets/Monsters/128x128/14_1.png)  | |   ||Owl lock|
